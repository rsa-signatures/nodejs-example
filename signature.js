const fs = require('fs')
const crypto = require('crypto')

try {
    const data = fs.readFileSync('input.json', 'utf8');
    const compactJson = JSON.stringify(JSON.parse(data))
    console.log(compactJson);
    console.log()

    const pKey = crypto.createPrivateKey({
        key: Buffer.from(fs.readFileSync('domain.key', 'utf8')),
        passphrase: '1234'
    })
    const signature = crypto.sign('SHA256', Buffer.from(compactJson), pKey);
    console.log(signature.toString('base64'));
} catch (err) {
    console.error(err);
}

